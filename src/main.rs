extern crate binaryen;
extern crate clang;

use binaryen::*;
use clang::*;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;

struct BytecodeUnit {
    function_table: HashMap<String, FnType>,
    last_fn: Option<String>
}

fn ctype_to_wasm_valtype(ctype: TypeKind) -> ValueTy {
    match ctype {
        TypeKind::Pointer | TypeKind::Int => ValueTy::I32,
        _ => unimplemented!(),
    }
}

fn get_param_types(entity: &Entity) -> Vec<ValueTy> {
    let mut output: Vec<ValueTy> = Vec::new();
    for child in entity.get_children().iter() {
        if child.get_kind() != EntityKind::ParmDecl {
            continue;
        }
        output.push(ctype_to_wasm_valtype(
            child
                .get_type()
                .expect("Parameter is missing type")
                .get_kind(),
        ));
    }
    output
}

fn parse_body(module: &Module, entity: &Entity, unit: &mut BytecodeUnit) {
    module.switch()
}

fn parse_function(module: &Module, entity: &Entity, unit: &mut BytecodeUnit) {
    let func_name = entity.get_name().unwrap_or("UNKNOWN".to_string());
    if unit.function_table.contains_key(&func_name) {
        return;
    }

    println!("Adding function declaration: '{}'", &func_name);
    let params = get_param_types(entity);
    let anon_name: Option<String> = None;
    let fn_type = module.add_fn_type(anon_name, &params, Ty::None);

    if !entity.is_definition() {
        println!("Import function: '{}'", &func_name);
        module.add_fn_import(
            func_name.as_str(),
            "sys",
            func_name.as_str(),
            &fn_type
        )
    }

    unit.function_table.insert(
        func_name.clone(),
        fn_type,
    );

    unit.last_fn = Some(func_name);
}

fn parse_entity(module: &Module, entity: &Entity, unit: &mut BytecodeUnit) {
    //println!("Parsing {:?}", entity);

    match entity.get_kind() {
        EntityKind::TranslationUnit => parse_entity_children(module, entity, unit),
        EntityKind::FunctionDecl => {
            parse_function(module, entity, unit);
            parse_entity_children(module, entity, unit)
        }
        EntityKind::CompoundStmt => {
            parse_body(module, entity, unit);
            parse_entity_children(module, entity, unit)
        },
        EntityKind::SwitchStmt => parse_entity_children(module, entity, unit),
        EntityKind::ReturnStmt => parse_entity_children(module, entity, unit),
        EntityKind::UnexposedExpr => parse_entity_children(module, entity, unit),
        EntityKind::CaseStmt => parse_entity_children(module, entity, unit),
        EntityKind::CallExpr => parse_entity_children(module, entity, unit),
        EntityKind::BreakStmt => parse_entity_children(module, entity, unit),
        EntityKind::DefaultStmt => parse_entity_children(module, entity, unit),
        EntityKind::ParmDecl => (), // nop because we already have the params
        _ => println!("WARN: Ignoring {:?}", entity.get_kind()),
    }
}

fn parse_entity_children(
    module: &Module,
    entity: &Entity,
    unit: &mut BytecodeUnit,
) {
    for child in entity.get_children() {
        parse_entity(module, &child, unit);
    }
}

fn main() {
    let mut unit = BytecodeUnit {
        function_table: HashMap::new(),
        last_fn: None,
    };
    let clang = Clang::new().unwrap();
    let module = Module::new();
    let index = Index::new(&clang, false, false);
    let translation_unit = index.parser("test/simple/g_main.c").parse().unwrap();
    let root = translation_unit.get_entity();
    parse_entity(&module, &root, &mut unit);
    /*



    let voidparams: &[ValueTy;0] = &[];
    let maintype = module.add_fn_type(Some("main"), voidparams, Ty::None);
    let mainfn = module.add_fn("_main", &maintype, &[], module.nop());
    module.add_fn_export("_main","_main");
    assert!(module.is_valid());

    module.print();

    let mut file = File::create("g_main.wasm").unwrap();
    let data = module.write();
    if let Err(e) = file.write_all(&data) {
        println!("{:?}",e)
    }
*/
}
/*
    // Acquire an instance of `Clang`
    let clang = Clang::new().unwrap();

    // Create a new `Index`
    let index = Index::new(&clang, false, false);

    // Parse a source file into a translation unit
    let tu = index.parser("test/simple/g_main.c").parse().unwrap();

    // Get the structs in this translation unit
    let children = tu
        .get_entity()
        .get_children()
        .into_iter()
        //.filter(|e| e.get_kind() == EntityKind::StructDecl)
        .collect::<Vec<_>>();
    for child in children {
        let ctype = child.get_type().unwrap();
        println!("Entity kind: {:?}",child.get_kind());
        for funcchild in child.get_children() {
            println!("\tEntity kind: {:?}",funcchild.get_kind());
            if let Some(internaltype) = funcchild.get_type() {
                println!("\t{} => {:?}", funcchild.get_name().unwrap_or("unknown".to_string()), internaltype);
            }
            else {
                println!("\t{} => ???", funcchild.get_name().unwrap_or("unknown".to_string()));
            }
        }
    }
*/
/*
    // Print information about the structs
    for struct_ in structs {
        let type_ = struct_.get_type().unwrap();
        let size = type_.get_sizeof().unwrap();
        println!(
            "struct: {:?} (size: {} bytes)",
            struct_.get_name().unwrap(),
            size
        );

        for field in struct_.get_children() {
            let name = field.get_name().unwrap();
            let offset = type_.get_offsetof(&name).unwrap();
            println!("    field: {:?} (offset: {} bits)", name, offset);
        }
    }*/
