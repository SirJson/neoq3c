# neoq3c

Hello stranger! You found one of my research projects. I was wondering if you could make Quake 3 WebAssembly instead of Quake 3 Bytecode.

TLDR: It's a stupid idea because it defeats the point of using the Quake 3 VM.

The motivation comes from the fact that the original compiler for Quake 3 Bytecode (lcc) is quite ancient at this point and produces poorly Bytecode results. So why not use clang and LLVM to produce platform independent optimized bytecode?

If I chose to continue with this project I will probably build the VM part on top of a forked [wac](https://github.com/elcritch/wac/tree/master) version which looks like the perfect fit to replace the old Quake 3 VM.

To avoid pulling all of emscripten (which is basically an emulated Unix environment for the Web) into your Quake 3 game code you would need an compiler like this:

https://github.com/yurydelendik/wasmception

.. or if you want you could use any other language that targets WebAssembly like Rust or even something like TypeScript.

## But isn't WebAssembly for the Web?

Despide the name "WebAssembly" the specification does not yet include anything web specific like DOM access or GC access. Even though they promised to add this later they also want to keep supporting Non-Web targets.
If you want to learn more about this have a look [here](https://webassembly.org/docs/non-web/)